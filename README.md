# MyLittleDice

Webapp permettant de lancer des dés virtuels. Ecrit avec JavaScript & Bootstrap. 

Démo disponible ici : [https://extrem-network.com/dev/projects/mylittledice](https://extrem-network.com/dev/projects/mylittledice)